package main

import (
	"context"
	"encoding/json"
	"fmt"

	api "gitlab.com/epidemics/service/api/go/v1"
)

func (s Service) ReportNewSymptoms(ctx context.Context, in *api.NewSymptomsReport) (*api.ReportNewSymptomsResponse, error) {
	symptoms := []string{}
	for _, s := range in.Symptoms {
		symptoms = append(symptoms, s.String())
	}

	fmt.Println(in.Symptoms, symptoms)

	jsymptoms, err := json.Marshal(symptoms)
	if err != nil {
		fmt.Println("marshal ", err)
		return nil, err
	}

	report := NewSymptomsReport{
		DeviceId: in.DeviceId,
		Symptoms: jsymptoms,
		ReportedAtUnixSeconds: in.ReportedAtUnixSeconds,
	}

	if err := s.db.AddNewSymptomsReport(report); err != nil {
		fmt.Println("insert ", err)
		return nil, err
	}

	return &api.ReportNewSymptomsResponse{}, nil
}

func (s Service) ReportRecovery(ctx context.Context, in *api.SymptomsRecoveryReport) (*api.ReportRecoveryResponse, error) {
	report := SymptomsRecoveryReport{
		DeviceId: in.DeviceId,
		ReportedAtUnixSeconds: in.ReportedAtUnixSeconds,
	}

	if err := s.db.AddSymptomsRecoveryReport(report); err != nil {
		return nil, err
	}

	return &api.ReportRecoveryResponse{}, nil
}

func (s Service) CountCasesWithinRadius(ctx context.Context, in *api.CountCasesWithinRadiusRequest) ( *api.CountCasesWithinRadiusResponse, error) {
	count, err := s.db.CountPointsWithinRadius(in.Longitude, in.Latitude, in.RadiusMeters)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	fmt.Println("count: ", count)
	return &api.CountCasesWithinRadiusResponse{Count: int32(count)}, nil
}

func (s Service) ReportPosition(ctx context.Context, in *api.PositionObservation) (*api.ReportPositionResponse, error) {
	observation := PositionObservation{
		DeviceId: in.DeviceId,
		AccuracyMeters: in.AccuracyMeters,
		LongitudeDegrees: in.LongitudeDegrees,
		LatitudeDegrees: in.LatitudeDegrees,
		SpeedMetersPerSecond: in.SpeedMetersPerSecond,
		TimestampUnixSeconds: in.TimestampUnixSeconds,
	}

	fmt.Println(observation)

	if err := s.db.AddPositionObservation(observation); err != nil {
		fmt.Println(err)
		return nil, err
	}

	return &api.ReportPositionResponse{}, nil
}
