package io.gitlab.epidemics.api.v1;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.28.1)",
    comments = "Source: v1/api.proto")
public final class EpidemicsAPIGrpc {

  private EpidemicsAPIGrpc() {}

  public static final String SERVICE_NAME = "v1.EpidemicsAPI";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.NewSymptomsReport,
      io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse> getReportNewSymptomsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ReportNewSymptoms",
      requestType = io.gitlab.epidemics.api.v1.Api.NewSymptomsReport.class,
      responseType = io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.NewSymptomsReport,
      io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse> getReportNewSymptomsMethod() {
    io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.NewSymptomsReport, io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse> getReportNewSymptomsMethod;
    if ((getReportNewSymptomsMethod = EpidemicsAPIGrpc.getReportNewSymptomsMethod) == null) {
      synchronized (EpidemicsAPIGrpc.class) {
        if ((getReportNewSymptomsMethod = EpidemicsAPIGrpc.getReportNewSymptomsMethod) == null) {
          EpidemicsAPIGrpc.getReportNewSymptomsMethod = getReportNewSymptomsMethod =
              io.grpc.MethodDescriptor.<io.gitlab.epidemics.api.v1.Api.NewSymptomsReport, io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ReportNewSymptoms"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  io.gitlab.epidemics.api.v1.Api.NewSymptomsReport.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse.getDefaultInstance()))
              .setSchemaDescriptor(new EpidemicsAPIMethodDescriptorSupplier("ReportNewSymptoms"))
              .build();
        }
      }
    }
    return getReportNewSymptomsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport,
      io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse> getReportRecoveryMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ReportRecovery",
      requestType = io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport.class,
      responseType = io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport,
      io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse> getReportRecoveryMethod() {
    io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport, io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse> getReportRecoveryMethod;
    if ((getReportRecoveryMethod = EpidemicsAPIGrpc.getReportRecoveryMethod) == null) {
      synchronized (EpidemicsAPIGrpc.class) {
        if ((getReportRecoveryMethod = EpidemicsAPIGrpc.getReportRecoveryMethod) == null) {
          EpidemicsAPIGrpc.getReportRecoveryMethod = getReportRecoveryMethod =
              io.grpc.MethodDescriptor.<io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport, io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ReportRecovery"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse.getDefaultInstance()))
              .setSchemaDescriptor(new EpidemicsAPIMethodDescriptorSupplier("ReportRecovery"))
              .build();
        }
      }
    }
    return getReportRecoveryMethod;
  }

  private static volatile io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.PositionObservation,
      io.gitlab.epidemics.api.v1.Api.ReportPositionResponse> getReportPositionMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ReportPosition",
      requestType = io.gitlab.epidemics.api.v1.Api.PositionObservation.class,
      responseType = io.gitlab.epidemics.api.v1.Api.ReportPositionResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.PositionObservation,
      io.gitlab.epidemics.api.v1.Api.ReportPositionResponse> getReportPositionMethod() {
    io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.PositionObservation, io.gitlab.epidemics.api.v1.Api.ReportPositionResponse> getReportPositionMethod;
    if ((getReportPositionMethod = EpidemicsAPIGrpc.getReportPositionMethod) == null) {
      synchronized (EpidemicsAPIGrpc.class) {
        if ((getReportPositionMethod = EpidemicsAPIGrpc.getReportPositionMethod) == null) {
          EpidemicsAPIGrpc.getReportPositionMethod = getReportPositionMethod =
              io.grpc.MethodDescriptor.<io.gitlab.epidemics.api.v1.Api.PositionObservation, io.gitlab.epidemics.api.v1.Api.ReportPositionResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ReportPosition"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  io.gitlab.epidemics.api.v1.Api.PositionObservation.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  io.gitlab.epidemics.api.v1.Api.ReportPositionResponse.getDefaultInstance()))
              .setSchemaDescriptor(new EpidemicsAPIMethodDescriptorSupplier("ReportPosition"))
              .build();
        }
      }
    }
    return getReportPositionMethod;
  }

  private static volatile io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest,
      io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse> getCountCasesWithinRadiusMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "CountCasesWithinRadius",
      requestType = io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest.class,
      responseType = io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest,
      io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse> getCountCasesWithinRadiusMethod() {
    io.grpc.MethodDescriptor<io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest, io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse> getCountCasesWithinRadiusMethod;
    if ((getCountCasesWithinRadiusMethod = EpidemicsAPIGrpc.getCountCasesWithinRadiusMethod) == null) {
      synchronized (EpidemicsAPIGrpc.class) {
        if ((getCountCasesWithinRadiusMethod = EpidemicsAPIGrpc.getCountCasesWithinRadiusMethod) == null) {
          EpidemicsAPIGrpc.getCountCasesWithinRadiusMethod = getCountCasesWithinRadiusMethod =
              io.grpc.MethodDescriptor.<io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest, io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "CountCasesWithinRadius"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse.getDefaultInstance()))
              .setSchemaDescriptor(new EpidemicsAPIMethodDescriptorSupplier("CountCasesWithinRadius"))
              .build();
        }
      }
    }
    return getCountCasesWithinRadiusMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static EpidemicsAPIStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<EpidemicsAPIStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<EpidemicsAPIStub>() {
        @java.lang.Override
        public EpidemicsAPIStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new EpidemicsAPIStub(channel, callOptions);
        }
      };
    return EpidemicsAPIStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static EpidemicsAPIBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<EpidemicsAPIBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<EpidemicsAPIBlockingStub>() {
        @java.lang.Override
        public EpidemicsAPIBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new EpidemicsAPIBlockingStub(channel, callOptions);
        }
      };
    return EpidemicsAPIBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static EpidemicsAPIFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<EpidemicsAPIFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<EpidemicsAPIFutureStub>() {
        @java.lang.Override
        public EpidemicsAPIFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new EpidemicsAPIFutureStub(channel, callOptions);
        }
      };
    return EpidemicsAPIFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class EpidemicsAPIImplBase implements io.grpc.BindableService {

    /**
     */
    public void reportNewSymptoms(io.gitlab.epidemics.api.v1.Api.NewSymptomsReport request,
        io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getReportNewSymptomsMethod(), responseObserver);
    }

    /**
     */
    public void reportRecovery(io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport request,
        io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getReportRecoveryMethod(), responseObserver);
    }

    /**
     */
    public void reportPosition(io.gitlab.epidemics.api.v1.Api.PositionObservation request,
        io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.ReportPositionResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getReportPositionMethod(), responseObserver);
    }

    /**
     */
    public void countCasesWithinRadius(io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest request,
        io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getCountCasesWithinRadiusMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getReportNewSymptomsMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                io.gitlab.epidemics.api.v1.Api.NewSymptomsReport,
                io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse>(
                  this, METHODID_REPORT_NEW_SYMPTOMS)))
          .addMethod(
            getReportRecoveryMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport,
                io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse>(
                  this, METHODID_REPORT_RECOVERY)))
          .addMethod(
            getReportPositionMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                io.gitlab.epidemics.api.v1.Api.PositionObservation,
                io.gitlab.epidemics.api.v1.Api.ReportPositionResponse>(
                  this, METHODID_REPORT_POSITION)))
          .addMethod(
            getCountCasesWithinRadiusMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest,
                io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse>(
                  this, METHODID_COUNT_CASES_WITHIN_RADIUS)))
          .build();
    }
  }

  /**
   */
  public static final class EpidemicsAPIStub extends io.grpc.stub.AbstractAsyncStub<EpidemicsAPIStub> {
    private EpidemicsAPIStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected EpidemicsAPIStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new EpidemicsAPIStub(channel, callOptions);
    }

    /**
     */
    public void reportNewSymptoms(io.gitlab.epidemics.api.v1.Api.NewSymptomsReport request,
        io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getReportNewSymptomsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void reportRecovery(io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport request,
        io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getReportRecoveryMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void reportPosition(io.gitlab.epidemics.api.v1.Api.PositionObservation request,
        io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.ReportPositionResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getReportPositionMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void countCasesWithinRadius(io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest request,
        io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCountCasesWithinRadiusMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class EpidemicsAPIBlockingStub extends io.grpc.stub.AbstractBlockingStub<EpidemicsAPIBlockingStub> {
    private EpidemicsAPIBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected EpidemicsAPIBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new EpidemicsAPIBlockingStub(channel, callOptions);
    }

    /**
     */
    public io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse reportNewSymptoms(io.gitlab.epidemics.api.v1.Api.NewSymptomsReport request) {
      return blockingUnaryCall(
          getChannel(), getReportNewSymptomsMethod(), getCallOptions(), request);
    }

    /**
     */
    public io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse reportRecovery(io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport request) {
      return blockingUnaryCall(
          getChannel(), getReportRecoveryMethod(), getCallOptions(), request);
    }

    /**
     */
    public io.gitlab.epidemics.api.v1.Api.ReportPositionResponse reportPosition(io.gitlab.epidemics.api.v1.Api.PositionObservation request) {
      return blockingUnaryCall(
          getChannel(), getReportPositionMethod(), getCallOptions(), request);
    }

    /**
     */
    public io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse countCasesWithinRadius(io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest request) {
      return blockingUnaryCall(
          getChannel(), getCountCasesWithinRadiusMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class EpidemicsAPIFutureStub extends io.grpc.stub.AbstractFutureStub<EpidemicsAPIFutureStub> {
    private EpidemicsAPIFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected EpidemicsAPIFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new EpidemicsAPIFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse> reportNewSymptoms(
        io.gitlab.epidemics.api.v1.Api.NewSymptomsReport request) {
      return futureUnaryCall(
          getChannel().newCall(getReportNewSymptomsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse> reportRecovery(
        io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport request) {
      return futureUnaryCall(
          getChannel().newCall(getReportRecoveryMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<io.gitlab.epidemics.api.v1.Api.ReportPositionResponse> reportPosition(
        io.gitlab.epidemics.api.v1.Api.PositionObservation request) {
      return futureUnaryCall(
          getChannel().newCall(getReportPositionMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse> countCasesWithinRadius(
        io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getCountCasesWithinRadiusMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_REPORT_NEW_SYMPTOMS = 0;
  private static final int METHODID_REPORT_RECOVERY = 1;
  private static final int METHODID_REPORT_POSITION = 2;
  private static final int METHODID_COUNT_CASES_WITHIN_RADIUS = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final EpidemicsAPIImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(EpidemicsAPIImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_REPORT_NEW_SYMPTOMS:
          serviceImpl.reportNewSymptoms((io.gitlab.epidemics.api.v1.Api.NewSymptomsReport) request,
              (io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.ReportNewSymptomsResponse>) responseObserver);
          break;
        case METHODID_REPORT_RECOVERY:
          serviceImpl.reportRecovery((io.gitlab.epidemics.api.v1.Api.SymptomsRecoveryReport) request,
              (io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.ReportRecoveryResponse>) responseObserver);
          break;
        case METHODID_REPORT_POSITION:
          serviceImpl.reportPosition((io.gitlab.epidemics.api.v1.Api.PositionObservation) request,
              (io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.ReportPositionResponse>) responseObserver);
          break;
        case METHODID_COUNT_CASES_WITHIN_RADIUS:
          serviceImpl.countCasesWithinRadius((io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusRequest) request,
              (io.grpc.stub.StreamObserver<io.gitlab.epidemics.api.v1.Api.CountCasesWithinRadiusResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class EpidemicsAPIBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    EpidemicsAPIBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return io.gitlab.epidemics.api.v1.Api.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("EpidemicsAPI");
    }
  }

  private static final class EpidemicsAPIFileDescriptorSupplier
      extends EpidemicsAPIBaseDescriptorSupplier {
    EpidemicsAPIFileDescriptorSupplier() {}
  }

  private static final class EpidemicsAPIMethodDescriptorSupplier
      extends EpidemicsAPIBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    EpidemicsAPIMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (EpidemicsAPIGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new EpidemicsAPIFileDescriptorSupplier())
              .addMethod(getReportNewSymptomsMethod())
              .addMethod(getReportRecoveryMethod())
              .addMethod(getReportPositionMethod())
              .addMethod(getCountCasesWithinRadiusMethod())
              .build();
        }
      }
    }
    return result;
  }
}
