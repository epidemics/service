// Code generated by protoc-gen-go. DO NOT EDIT.
// source: v1/api.proto

package v1

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Symptom int32

const (
	Symptom_FEVER                    Symptom = 0
	Symptom_DRY_COUGH                Symptom = 1
	Symptom_TIREDNESS                Symptom = 2
	Symptom_ACHES_AND_PAINS          Symptom = 3
	Symptom_SORE_THROAT              Symptom = 4
	Symptom_DIARRHOEA                Symptom = 5
	Symptom_CONJUNCTIVITIS           Symptom = 6
	Symptom_HEADACHE                 Symptom = 7
	Symptom_LOSS_OF_TASTE_OR_SMELL   Symptom = 8
	Symptom_SKIN_RASHES              Symptom = 9
	Symptom_DISCOLORATION_OF_FINGERS Symptom = 10
	Symptom_BREATH_DIFFICULTY        Symptom = 11
	Symptom_CHEST_PAIN               Symptom = 12
	Symptom_LOSS_OF_SPEECH           Symptom = 13
	Symptom_LOSS_OF_MOVEMENT         Symptom = 14
)

var Symptom_name = map[int32]string{
	0:  "FEVER",
	1:  "DRY_COUGH",
	2:  "TIREDNESS",
	3:  "ACHES_AND_PAINS",
	4:  "SORE_THROAT",
	5:  "DIARRHOEA",
	6:  "CONJUNCTIVITIS",
	7:  "HEADACHE",
	8:  "LOSS_OF_TASTE_OR_SMELL",
	9:  "SKIN_RASHES",
	10: "DISCOLORATION_OF_FINGERS",
	11: "BREATH_DIFFICULTY",
	12: "CHEST_PAIN",
	13: "LOSS_OF_SPEECH",
	14: "LOSS_OF_MOVEMENT",
}

var Symptom_value = map[string]int32{
	"FEVER":                    0,
	"DRY_COUGH":                1,
	"TIREDNESS":                2,
	"ACHES_AND_PAINS":          3,
	"SORE_THROAT":              4,
	"DIARRHOEA":                5,
	"CONJUNCTIVITIS":           6,
	"HEADACHE":                 7,
	"LOSS_OF_TASTE_OR_SMELL":   8,
	"SKIN_RASHES":              9,
	"DISCOLORATION_OF_FINGERS": 10,
	"BREATH_DIFFICULTY":        11,
	"CHEST_PAIN":               12,
	"LOSS_OF_SPEECH":           13,
	"LOSS_OF_MOVEMENT":         14,
}

func (x Symptom) String() string {
	return proto.EnumName(Symptom_name, int32(x))
}

func (Symptom) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_c9066d184c356ed3, []int{0}
}

type CountCasesWithinRadiusRequest struct {
	Latitude             float64  `protobuf:"fixed64,1,opt,name=latitude,proto3" json:"latitude,omitempty"`
	Longitude            float64  `protobuf:"fixed64,2,opt,name=longitude,proto3" json:"longitude,omitempty"`
	RadiusMeters         float64  `protobuf:"fixed64,3,opt,name=radius_meters,json=radiusMeters,proto3" json:"radius_meters,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CountCasesWithinRadiusRequest) Reset()         { *m = CountCasesWithinRadiusRequest{} }
func (m *CountCasesWithinRadiusRequest) String() string { return proto.CompactTextString(m) }
func (*CountCasesWithinRadiusRequest) ProtoMessage()    {}
func (*CountCasesWithinRadiusRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_c9066d184c356ed3, []int{0}
}

func (m *CountCasesWithinRadiusRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CountCasesWithinRadiusRequest.Unmarshal(m, b)
}
func (m *CountCasesWithinRadiusRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CountCasesWithinRadiusRequest.Marshal(b, m, deterministic)
}
func (m *CountCasesWithinRadiusRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CountCasesWithinRadiusRequest.Merge(m, src)
}
func (m *CountCasesWithinRadiusRequest) XXX_Size() int {
	return xxx_messageInfo_CountCasesWithinRadiusRequest.Size(m)
}
func (m *CountCasesWithinRadiusRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CountCasesWithinRadiusRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CountCasesWithinRadiusRequest proto.InternalMessageInfo

func (m *CountCasesWithinRadiusRequest) GetLatitude() float64 {
	if m != nil {
		return m.Latitude
	}
	return 0
}

func (m *CountCasesWithinRadiusRequest) GetLongitude() float64 {
	if m != nil {
		return m.Longitude
	}
	return 0
}

func (m *CountCasesWithinRadiusRequest) GetRadiusMeters() float64 {
	if m != nil {
		return m.RadiusMeters
	}
	return 0
}

type CountCasesWithinRadiusResponse struct {
	Count                int32    `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CountCasesWithinRadiusResponse) Reset()         { *m = CountCasesWithinRadiusResponse{} }
func (m *CountCasesWithinRadiusResponse) String() string { return proto.CompactTextString(m) }
func (*CountCasesWithinRadiusResponse) ProtoMessage()    {}
func (*CountCasesWithinRadiusResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_c9066d184c356ed3, []int{1}
}

func (m *CountCasesWithinRadiusResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CountCasesWithinRadiusResponse.Unmarshal(m, b)
}
func (m *CountCasesWithinRadiusResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CountCasesWithinRadiusResponse.Marshal(b, m, deterministic)
}
func (m *CountCasesWithinRadiusResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CountCasesWithinRadiusResponse.Merge(m, src)
}
func (m *CountCasesWithinRadiusResponse) XXX_Size() int {
	return xxx_messageInfo_CountCasesWithinRadiusResponse.Size(m)
}
func (m *CountCasesWithinRadiusResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CountCasesWithinRadiusResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CountCasesWithinRadiusResponse proto.InternalMessageInfo

func (m *CountCasesWithinRadiusResponse) GetCount() int32 {
	if m != nil {
		return m.Count
	}
	return 0
}

type ReportNewSymptomsResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ReportNewSymptomsResponse) Reset()         { *m = ReportNewSymptomsResponse{} }
func (m *ReportNewSymptomsResponse) String() string { return proto.CompactTextString(m) }
func (*ReportNewSymptomsResponse) ProtoMessage()    {}
func (*ReportNewSymptomsResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_c9066d184c356ed3, []int{2}
}

func (m *ReportNewSymptomsResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ReportNewSymptomsResponse.Unmarshal(m, b)
}
func (m *ReportNewSymptomsResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ReportNewSymptomsResponse.Marshal(b, m, deterministic)
}
func (m *ReportNewSymptomsResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReportNewSymptomsResponse.Merge(m, src)
}
func (m *ReportNewSymptomsResponse) XXX_Size() int {
	return xxx_messageInfo_ReportNewSymptomsResponse.Size(m)
}
func (m *ReportNewSymptomsResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ReportNewSymptomsResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ReportNewSymptomsResponse proto.InternalMessageInfo

type ReportRecoveryResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ReportRecoveryResponse) Reset()         { *m = ReportRecoveryResponse{} }
func (m *ReportRecoveryResponse) String() string { return proto.CompactTextString(m) }
func (*ReportRecoveryResponse) ProtoMessage()    {}
func (*ReportRecoveryResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_c9066d184c356ed3, []int{3}
}

func (m *ReportRecoveryResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ReportRecoveryResponse.Unmarshal(m, b)
}
func (m *ReportRecoveryResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ReportRecoveryResponse.Marshal(b, m, deterministic)
}
func (m *ReportRecoveryResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReportRecoveryResponse.Merge(m, src)
}
func (m *ReportRecoveryResponse) XXX_Size() int {
	return xxx_messageInfo_ReportRecoveryResponse.Size(m)
}
func (m *ReportRecoveryResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ReportRecoveryResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ReportRecoveryResponse proto.InternalMessageInfo

type ReportPositionResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ReportPositionResponse) Reset()         { *m = ReportPositionResponse{} }
func (m *ReportPositionResponse) String() string { return proto.CompactTextString(m) }
func (*ReportPositionResponse) ProtoMessage()    {}
func (*ReportPositionResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_c9066d184c356ed3, []int{4}
}

func (m *ReportPositionResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ReportPositionResponse.Unmarshal(m, b)
}
func (m *ReportPositionResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ReportPositionResponse.Marshal(b, m, deterministic)
}
func (m *ReportPositionResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReportPositionResponse.Merge(m, src)
}
func (m *ReportPositionResponse) XXX_Size() int {
	return xxx_messageInfo_ReportPositionResponse.Size(m)
}
func (m *ReportPositionResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ReportPositionResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ReportPositionResponse proto.InternalMessageInfo

type PositionObservation struct {
	DeviceId             string   `protobuf:"bytes,1,opt,name=device_id,json=deviceId,proto3" json:"device_id,omitempty"`
	AccuracyMeters       float64  `protobuf:"fixed64,2,opt,name=accuracy_meters,json=accuracyMeters,proto3" json:"accuracy_meters,omitempty"`
	AltitudeMeters       float64  `protobuf:"fixed64,3,opt,name=altitude_meters,json=altitudeMeters,proto3" json:"altitude_meters,omitempty"`
	LongitudeDegrees     float64  `protobuf:"fixed64,4,opt,name=longitude_degrees,json=longitudeDegrees,proto3" json:"longitude_degrees,omitempty"`
	LatitudeDegrees      float64  `protobuf:"fixed64,5,opt,name=latitude_degrees,json=latitudeDegrees,proto3" json:"latitude_degrees,omitempty"`
	SpeedMetersPerSecond float64  `protobuf:"fixed64,6,opt,name=speed_meters_per_second,json=speedMetersPerSecond,proto3" json:"speed_meters_per_second,omitempty"`
	TimestampUnixSeconds int64    `protobuf:"varint,7,opt,name=timestamp_unix_seconds,json=timestampUnixSeconds,proto3" json:"timestamp_unix_seconds,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PositionObservation) Reset()         { *m = PositionObservation{} }
func (m *PositionObservation) String() string { return proto.CompactTextString(m) }
func (*PositionObservation) ProtoMessage()    {}
func (*PositionObservation) Descriptor() ([]byte, []int) {
	return fileDescriptor_c9066d184c356ed3, []int{5}
}

func (m *PositionObservation) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PositionObservation.Unmarshal(m, b)
}
func (m *PositionObservation) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PositionObservation.Marshal(b, m, deterministic)
}
func (m *PositionObservation) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PositionObservation.Merge(m, src)
}
func (m *PositionObservation) XXX_Size() int {
	return xxx_messageInfo_PositionObservation.Size(m)
}
func (m *PositionObservation) XXX_DiscardUnknown() {
	xxx_messageInfo_PositionObservation.DiscardUnknown(m)
}

var xxx_messageInfo_PositionObservation proto.InternalMessageInfo

func (m *PositionObservation) GetDeviceId() string {
	if m != nil {
		return m.DeviceId
	}
	return ""
}

func (m *PositionObservation) GetAccuracyMeters() float64 {
	if m != nil {
		return m.AccuracyMeters
	}
	return 0
}

func (m *PositionObservation) GetAltitudeMeters() float64 {
	if m != nil {
		return m.AltitudeMeters
	}
	return 0
}

func (m *PositionObservation) GetLongitudeDegrees() float64 {
	if m != nil {
		return m.LongitudeDegrees
	}
	return 0
}

func (m *PositionObservation) GetLatitudeDegrees() float64 {
	if m != nil {
		return m.LatitudeDegrees
	}
	return 0
}

func (m *PositionObservation) GetSpeedMetersPerSecond() float64 {
	if m != nil {
		return m.SpeedMetersPerSecond
	}
	return 0
}

func (m *PositionObservation) GetTimestampUnixSeconds() int64 {
	if m != nil {
		return m.TimestampUnixSeconds
	}
	return 0
}

type NewSymptomsReport struct {
	DeviceId              string    `protobuf:"bytes,1,opt,name=device_id,json=deviceId,proto3" json:"device_id,omitempty"`
	Symptoms              []Symptom `protobuf:"varint,2,rep,packed,name=symptoms,proto3,enum=v1.Symptom" json:"symptoms,omitempty"`
	ReportedAtUnixSeconds int64     `protobuf:"varint,3,opt,name=reported_at_unix_seconds,json=reportedAtUnixSeconds,proto3" json:"reported_at_unix_seconds,omitempty"`
	XXX_NoUnkeyedLiteral  struct{}  `json:"-"`
	XXX_unrecognized      []byte    `json:"-"`
	XXX_sizecache         int32     `json:"-"`
}

func (m *NewSymptomsReport) Reset()         { *m = NewSymptomsReport{} }
func (m *NewSymptomsReport) String() string { return proto.CompactTextString(m) }
func (*NewSymptomsReport) ProtoMessage()    {}
func (*NewSymptomsReport) Descriptor() ([]byte, []int) {
	return fileDescriptor_c9066d184c356ed3, []int{6}
}

func (m *NewSymptomsReport) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_NewSymptomsReport.Unmarshal(m, b)
}
func (m *NewSymptomsReport) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_NewSymptomsReport.Marshal(b, m, deterministic)
}
func (m *NewSymptomsReport) XXX_Merge(src proto.Message) {
	xxx_messageInfo_NewSymptomsReport.Merge(m, src)
}
func (m *NewSymptomsReport) XXX_Size() int {
	return xxx_messageInfo_NewSymptomsReport.Size(m)
}
func (m *NewSymptomsReport) XXX_DiscardUnknown() {
	xxx_messageInfo_NewSymptomsReport.DiscardUnknown(m)
}

var xxx_messageInfo_NewSymptomsReport proto.InternalMessageInfo

func (m *NewSymptomsReport) GetDeviceId() string {
	if m != nil {
		return m.DeviceId
	}
	return ""
}

func (m *NewSymptomsReport) GetSymptoms() []Symptom {
	if m != nil {
		return m.Symptoms
	}
	return nil
}

func (m *NewSymptomsReport) GetReportedAtUnixSeconds() int64 {
	if m != nil {
		return m.ReportedAtUnixSeconds
	}
	return 0
}

type SymptomsRecoveryReport struct {
	DeviceId              string   `protobuf:"bytes,1,opt,name=device_id,json=deviceId,proto3" json:"device_id,omitempty"`
	ReportedAtUnixSeconds int64    `protobuf:"varint,2,opt,name=reported_at_unix_seconds,json=reportedAtUnixSeconds,proto3" json:"reported_at_unix_seconds,omitempty"`
	XXX_NoUnkeyedLiteral  struct{} `json:"-"`
	XXX_unrecognized      []byte   `json:"-"`
	XXX_sizecache         int32    `json:"-"`
}

func (m *SymptomsRecoveryReport) Reset()         { *m = SymptomsRecoveryReport{} }
func (m *SymptomsRecoveryReport) String() string { return proto.CompactTextString(m) }
func (*SymptomsRecoveryReport) ProtoMessage()    {}
func (*SymptomsRecoveryReport) Descriptor() ([]byte, []int) {
	return fileDescriptor_c9066d184c356ed3, []int{7}
}

func (m *SymptomsRecoveryReport) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SymptomsRecoveryReport.Unmarshal(m, b)
}
func (m *SymptomsRecoveryReport) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SymptomsRecoveryReport.Marshal(b, m, deterministic)
}
func (m *SymptomsRecoveryReport) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SymptomsRecoveryReport.Merge(m, src)
}
func (m *SymptomsRecoveryReport) XXX_Size() int {
	return xxx_messageInfo_SymptomsRecoveryReport.Size(m)
}
func (m *SymptomsRecoveryReport) XXX_DiscardUnknown() {
	xxx_messageInfo_SymptomsRecoveryReport.DiscardUnknown(m)
}

var xxx_messageInfo_SymptomsRecoveryReport proto.InternalMessageInfo

func (m *SymptomsRecoveryReport) GetDeviceId() string {
	if m != nil {
		return m.DeviceId
	}
	return ""
}

func (m *SymptomsRecoveryReport) GetReportedAtUnixSeconds() int64 {
	if m != nil {
		return m.ReportedAtUnixSeconds
	}
	return 0
}

func init() {
	proto.RegisterEnum("v1.Symptom", Symptom_name, Symptom_value)
	proto.RegisterType((*CountCasesWithinRadiusRequest)(nil), "v1.CountCasesWithinRadiusRequest")
	proto.RegisterType((*CountCasesWithinRadiusResponse)(nil), "v1.CountCasesWithinRadiusResponse")
	proto.RegisterType((*ReportNewSymptomsResponse)(nil), "v1.ReportNewSymptomsResponse")
	proto.RegisterType((*ReportRecoveryResponse)(nil), "v1.ReportRecoveryResponse")
	proto.RegisterType((*ReportPositionResponse)(nil), "v1.ReportPositionResponse")
	proto.RegisterType((*PositionObservation)(nil), "v1.PositionObservation")
	proto.RegisterType((*NewSymptomsReport)(nil), "v1.NewSymptomsReport")
	proto.RegisterType((*SymptomsRecoveryReport)(nil), "v1.SymptomsRecoveryReport")
}

func init() { proto.RegisterFile("v1/api.proto", fileDescriptor_c9066d184c356ed3) }

var fileDescriptor_c9066d184c356ed3 = []byte{
	// 852 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x84, 0x55, 0xcb, 0x72, 0xe3, 0x44,
	0x14, 0xc5, 0xf6, 0x38, 0xb1, 0x6f, 0x1c, 0xa7, 0xdd, 0x93, 0x38, 0x46, 0x49, 0xa8, 0x20, 0x16,
	0x84, 0x50, 0xe5, 0x94, 0x87, 0x57, 0xd5, 0xec, 0x34, 0x72, 0x7b, 0x2c, 0x70, 0x24, 0x57, 0x4b,
	0x09, 0x35, 0x6c, 0x54, 0x8a, 0xd4, 0x18, 0x55, 0xd9, 0x92, 0x50, 0xcb, 0x62, 0xb2, 0x61, 0xc1,
	0x27, 0xc0, 0x37, 0xf0, 0x1d, 0xac, 0x59, 0xf3, 0x0b, 0x7c, 0x08, 0xa5, 0x96, 0xdb, 0x49, 0xf0,
	0x4c, 0xb2, 0xec, 0x73, 0x4e, 0xdf, 0x73, 0x7d, 0xfb, 0xfa, 0x08, 0x5a, 0xf9, 0xe0, 0xc2, 0x4b,
	0xc2, 0x7e, 0x92, 0xc6, 0x59, 0x8c, 0xab, 0xf9, 0x40, 0x39, 0x9e, 0xc5, 0xf1, 0x6c, 0xce, 0x0a,
	0xf4, 0xc2, 0x8b, 0xa2, 0x38, 0xf3, 0xb2, 0x30, 0x8e, 0x78, 0xa9, 0x50, 0x7f, 0x85, 0x13, 0x3d,
	0x5e, 0x46, 0x99, 0xee, 0x71, 0xc6, 0xbf, 0x0f, 0xb3, 0x9f, 0xc2, 0x88, 0x7a, 0x41, 0xb8, 0xe4,
	0x94, 0xfd, 0xbc, 0x64, 0x3c, 0xc3, 0x0a, 0x34, 0xe6, 0x5e, 0x16, 0x66, 0xcb, 0x80, 0xf5, 0x2a,
	0xa7, 0x95, 0xb3, 0x0a, 0x5d, 0x9f, 0xf1, 0x31, 0x34, 0xe7, 0x71, 0x34, 0x2b, 0xc9, 0xaa, 0x20,
	0xef, 0x00, 0xfc, 0x09, 0xec, 0xa6, 0xa2, 0x94, 0xbb, 0x60, 0x19, 0x4b, 0x79, 0xaf, 0x26, 0x14,
	0xad, 0x12, 0xbc, 0x14, 0x98, 0xfa, 0x35, 0x7c, 0xf4, 0x3e, 0x7f, 0x9e, 0xc4, 0x11, 0x67, 0x78,
	0x1f, 0xea, 0x7e, 0xa1, 0x10, 0xee, 0x75, 0x5a, 0x1e, 0xd4, 0x23, 0xf8, 0x90, 0xb2, 0x24, 0x4e,
	0x33, 0x93, 0xfd, 0x62, 0xdf, 0x2e, 0x92, 0x2c, 0x5e, 0xac, 0xaf, 0xa8, 0x3d, 0xe8, 0x96, 0x24,
	0x65, 0x7e, 0x9c, 0xb3, 0xf4, 0x76, 0x93, 0x99, 0xc6, 0x3c, 0x2c, 0xe6, 0xb0, 0x66, 0xfe, 0xae,
	0xc2, 0x73, 0x09, 0x5a, 0x37, 0x9c, 0xa5, 0xb9, 0x98, 0x13, 0x3e, 0x82, 0x66, 0xc0, 0xf2, 0xd0,
	0x67, 0x6e, 0x18, 0x88, 0x16, 0x9a, 0xb4, 0x51, 0x02, 0x46, 0x80, 0x3f, 0x85, 0x3d, 0xcf, 0xf7,
	0x97, 0xa9, 0xe7, 0xdf, 0xca, 0x1f, 0x59, 0x8e, 0xa1, 0x2d, 0xe1, 0xf2, 0x67, 0x0a, 0xe1, 0xbc,
	0x9c, 0xda, 0xc3, 0x69, 0xb4, 0x25, 0xbc, 0x12, 0x7e, 0x0e, 0x9d, 0xf5, 0x04, 0xdd, 0x80, 0xcd,
	0x52, 0xc6, 0x78, 0xef, 0x99, 0x90, 0xa2, 0x35, 0x31, 0x2c, 0x71, 0xfc, 0x19, 0x20, 0xf9, 0x16,
	0x6b, 0x6d, 0x5d, 0x68, 0xf7, 0x24, 0x2e, 0xa5, 0x5f, 0xc1, 0x21, 0x4f, 0x18, 0x0b, 0x56, 0xee,
	0x6e, 0xc2, 0x52, 0x97, 0x33, 0x3f, 0x8e, 0x82, 0xde, 0x96, 0xb8, 0xb1, 0x2f, 0xe8, 0xb2, 0x8b,
	0x29, 0x4b, 0x6d, 0xc1, 0xe1, 0x2f, 0xa1, 0x9b, 0x85, 0x0b, 0xc6, 0x33, 0x6f, 0x91, 0xb8, 0xcb,
	0x28, 0x7c, 0xbb, 0xba, 0xc4, 0x7b, 0xdb, 0xa7, 0x95, 0xb3, 0x1a, 0xdd, 0x5f, 0xb3, 0x57, 0x51,
	0xf8, 0xb6, 0xbc, 0xc4, 0xd5, 0xdf, 0x2b, 0xd0, 0x79, 0xf0, 0x2e, 0xc5, 0xc4, 0x9f, 0x9a, 0x64,
	0x83, 0xaf, 0xe4, 0xbd, 0xea, 0x69, 0xed, 0xac, 0xfd, 0x62, 0xa7, 0x9f, 0x0f, 0xfa, 0xab, 0x12,
	0x74, 0x4d, 0xe2, 0x6f, 0xa0, 0x97, 0x8a, 0x7a, 0x2c, 0x70, 0xbd, 0xec, 0x61, 0x4f, 0x35, 0xd1,
	0xd3, 0x81, 0xe4, 0xb5, 0xec, 0x7e, 0x53, 0x11, 0x74, 0xef, 0x1a, 0x92, 0x6b, 0xf1, 0x74, 0x63,
	0x8f, 0xf9, 0x55, 0x1f, 0xf1, 0x3b, 0xff, 0xb3, 0x0a, 0xdb, 0x2b, 0x43, 0xdc, 0x84, 0xfa, 0x88,
	0x5c, 0x13, 0x8a, 0x3e, 0xc0, 0xbb, 0xd0, 0x1c, 0xd2, 0x37, 0xae, 0x6e, 0x5d, 0xbd, 0x1e, 0xa3,
	0x4a, 0x71, 0x74, 0x0c, 0x4a, 0x86, 0x26, 0xb1, 0x6d, 0x54, 0xc5, 0xcf, 0x61, 0x4f, 0xd3, 0xc7,
	0xc4, 0x76, 0x35, 0x73, 0xe8, 0x4e, 0x35, 0xc3, 0xb4, 0x51, 0x0d, 0xef, 0xc1, 0x8e, 0x6d, 0x51,
	0xe2, 0x3a, 0x63, 0x6a, 0x69, 0x0e, 0x7a, 0x26, 0x6a, 0x18, 0x1a, 0xa5, 0x63, 0x8b, 0x68, 0xa8,
	0x8e, 0x31, 0xb4, 0x75, 0xcb, 0xfc, 0xf6, 0xca, 0xd4, 0x1d, 0xe3, 0xda, 0x70, 0x0c, 0x1b, 0x6d,
	0xe1, 0x16, 0x34, 0xc6, 0x44, 0x1b, 0x16, 0xc5, 0xd0, 0x36, 0x56, 0xa0, 0x3b, 0xb1, 0x6c, 0xdb,
	0xb5, 0x46, 0xae, 0xa3, 0xd9, 0x0e, 0x71, 0x2d, 0xea, 0xda, 0x97, 0x64, 0x32, 0x41, 0x0d, 0x51,
	0xfd, 0x3b, 0xc3, 0x74, 0xa9, 0x66, 0x8f, 0x89, 0x8d, 0x9a, 0xf8, 0x18, 0x7a, 0x43, 0xc3, 0xd6,
	0xad, 0x89, 0x45, 0x35, 0xc7, 0xb0, 0xcc, 0xe2, 0xd6, 0xc8, 0x30, 0x5f, 0x13, 0x6a, 0x23, 0xc0,
	0x07, 0xd0, 0x79, 0x45, 0x89, 0xe6, 0x8c, 0xdd, 0xa1, 0x31, 0x1a, 0x19, 0xfa, 0xd5, 0xc4, 0x79,
	0x83, 0x76, 0x70, 0x1b, 0xa0, 0xe8, 0xdb, 0x11, 0x4d, 0xa3, 0x56, 0xd1, 0x93, 0x74, 0xb4, 0xa7,
	0x84, 0xe8, 0x63, 0xb4, 0x8b, 0xf7, 0x01, 0x49, 0xec, 0xd2, 0xba, 0x26, 0x97, 0xc4, 0x74, 0x50,
	0xfb, 0xc5, 0x5f, 0x35, 0x68, 0x91, 0x24, 0x0c, 0xd8, 0x22, 0xf4, 0xb9, 0x36, 0x35, 0xf0, 0x8f,
	0xd0, 0xd9, 0xf8, 0x6b, 0xe3, 0x83, 0x62, 0x1b, 0x36, 0x76, 0x4a, 0x39, 0x29, 0xe0, 0xf7, 0x07,
	0xc1, 0xd1, 0x6f, 0xff, 0xfc, 0xfb, 0x47, 0xf5, 0x40, 0x45, 0x22, 0xfd, 0xf2, 0xc1, 0x85, 0x5c,
	0xa3, 0x97, 0x95, 0x73, 0x3c, 0x83, 0xf6, 0xc3, 0x94, 0xc0, 0xca, 0xbd, 0x95, 0xfb, 0xdf, 0x92,
	0x28, 0xca, 0x9d, 0xd3, 0x46, 0xaa, 0x9c, 0x08, 0x9b, 0x43, 0x15, 0x4b, 0x9b, 0xb4, 0x54, 0x84,
	0x4c, 0x18, 0xf9, 0xd2, 0x48, 0xe6, 0x0b, 0x3e, 0x2c, 0x8a, 0xbd, 0x23, 0x6d, 0xee, 0xbb, 0x6c,
	0x24, 0xd4, 0xb1, 0x70, 0xe9, 0xaa, 0x1d, 0xe9, 0x92, 0xac, 0x14, 0xc2, 0xe4, 0x16, 0xba, 0xef,
	0x0e, 0x52, 0xfc, 0x71, 0x51, 0xf3, 0xd1, 0x90, 0x57, 0xd4, 0xc7, 0x24, 0x32, 0x3a, 0x85, 0x3d,
	0x56, 0x77, 0xa5, 0xbd, 0x5f, 0x48, 0x5f, 0x56, 0xce, 0x5f, 0x9d, 0x82, 0x12, 0xc6, 0xfd, 0x59,
	0x98, 0xcd, 0xbd, 0x9b, 0x3e, 0x93, 0x4f, 0xd9, 0x2f, 0x3e, 0x43, 0xf9, 0xe0, 0x87, 0x6a, 0x3e,
	0xb8, 0xd9, 0x12, 0x1f, 0x9b, 0x2f, 0xfe, 0x0b, 0x00, 0x00, 0xff, 0xff, 0x0a, 0x9c, 0xe0, 0x0f,
	0x9e, 0x06, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// EpidemicsAPIClient is the client API for EpidemicsAPI service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type EpidemicsAPIClient interface {
	ReportNewSymptoms(ctx context.Context, in *NewSymptomsReport, opts ...grpc.CallOption) (*ReportNewSymptomsResponse, error)
	ReportRecovery(ctx context.Context, in *SymptomsRecoveryReport, opts ...grpc.CallOption) (*ReportRecoveryResponse, error)
	ReportPosition(ctx context.Context, in *PositionObservation, opts ...grpc.CallOption) (*ReportPositionResponse, error)
	CountCasesWithinRadius(ctx context.Context, in *CountCasesWithinRadiusRequest, opts ...grpc.CallOption) (*CountCasesWithinRadiusResponse, error)
}

type epidemicsAPIClient struct {
	cc *grpc.ClientConn
}

func NewEpidemicsAPIClient(cc *grpc.ClientConn) EpidemicsAPIClient {
	return &epidemicsAPIClient{cc}
}

func (c *epidemicsAPIClient) ReportNewSymptoms(ctx context.Context, in *NewSymptomsReport, opts ...grpc.CallOption) (*ReportNewSymptomsResponse, error) {
	out := new(ReportNewSymptomsResponse)
	err := c.cc.Invoke(ctx, "/v1.EpidemicsAPI/ReportNewSymptoms", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *epidemicsAPIClient) ReportRecovery(ctx context.Context, in *SymptomsRecoveryReport, opts ...grpc.CallOption) (*ReportRecoveryResponse, error) {
	out := new(ReportRecoveryResponse)
	err := c.cc.Invoke(ctx, "/v1.EpidemicsAPI/ReportRecovery", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *epidemicsAPIClient) ReportPosition(ctx context.Context, in *PositionObservation, opts ...grpc.CallOption) (*ReportPositionResponse, error) {
	out := new(ReportPositionResponse)
	err := c.cc.Invoke(ctx, "/v1.EpidemicsAPI/ReportPosition", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *epidemicsAPIClient) CountCasesWithinRadius(ctx context.Context, in *CountCasesWithinRadiusRequest, opts ...grpc.CallOption) (*CountCasesWithinRadiusResponse, error) {
	out := new(CountCasesWithinRadiusResponse)
	err := c.cc.Invoke(ctx, "/v1.EpidemicsAPI/CountCasesWithinRadius", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// EpidemicsAPIServer is the server API for EpidemicsAPI service.
type EpidemicsAPIServer interface {
	ReportNewSymptoms(context.Context, *NewSymptomsReport) (*ReportNewSymptomsResponse, error)
	ReportRecovery(context.Context, *SymptomsRecoveryReport) (*ReportRecoveryResponse, error)
	ReportPosition(context.Context, *PositionObservation) (*ReportPositionResponse, error)
	CountCasesWithinRadius(context.Context, *CountCasesWithinRadiusRequest) (*CountCasesWithinRadiusResponse, error)
}

// UnimplementedEpidemicsAPIServer can be embedded to have forward compatible implementations.
type UnimplementedEpidemicsAPIServer struct {
}

func (*UnimplementedEpidemicsAPIServer) ReportNewSymptoms(ctx context.Context, req *NewSymptomsReport) (*ReportNewSymptomsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReportNewSymptoms not implemented")
}
func (*UnimplementedEpidemicsAPIServer) ReportRecovery(ctx context.Context, req *SymptomsRecoveryReport) (*ReportRecoveryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReportRecovery not implemented")
}
func (*UnimplementedEpidemicsAPIServer) ReportPosition(ctx context.Context, req *PositionObservation) (*ReportPositionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReportPosition not implemented")
}
func (*UnimplementedEpidemicsAPIServer) CountCasesWithinRadius(ctx context.Context, req *CountCasesWithinRadiusRequest) (*CountCasesWithinRadiusResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CountCasesWithinRadius not implemented")
}

func RegisterEpidemicsAPIServer(s *grpc.Server, srv EpidemicsAPIServer) {
	s.RegisterService(&_EpidemicsAPI_serviceDesc, srv)
}

func _EpidemicsAPI_ReportNewSymptoms_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NewSymptomsReport)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EpidemicsAPIServer).ReportNewSymptoms(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/v1.EpidemicsAPI/ReportNewSymptoms",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EpidemicsAPIServer).ReportNewSymptoms(ctx, req.(*NewSymptomsReport))
	}
	return interceptor(ctx, in, info, handler)
}

func _EpidemicsAPI_ReportRecovery_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SymptomsRecoveryReport)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EpidemicsAPIServer).ReportRecovery(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/v1.EpidemicsAPI/ReportRecovery",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EpidemicsAPIServer).ReportRecovery(ctx, req.(*SymptomsRecoveryReport))
	}
	return interceptor(ctx, in, info, handler)
}

func _EpidemicsAPI_ReportPosition_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PositionObservation)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EpidemicsAPIServer).ReportPosition(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/v1.EpidemicsAPI/ReportPosition",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EpidemicsAPIServer).ReportPosition(ctx, req.(*PositionObservation))
	}
	return interceptor(ctx, in, info, handler)
}

func _EpidemicsAPI_CountCasesWithinRadius_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CountCasesWithinRadiusRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EpidemicsAPIServer).CountCasesWithinRadius(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/v1.EpidemicsAPI/CountCasesWithinRadius",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EpidemicsAPIServer).CountCasesWithinRadius(ctx, req.(*CountCasesWithinRadiusRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _EpidemicsAPI_serviceDesc = grpc.ServiceDesc{
	ServiceName: "v1.EpidemicsAPI",
	HandlerType: (*EpidemicsAPIServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ReportNewSymptoms",
			Handler:    _EpidemicsAPI_ReportNewSymptoms_Handler,
		},
		{
			MethodName: "ReportRecovery",
			Handler:    _EpidemicsAPI_ReportRecovery_Handler,
		},
		{
			MethodName: "ReportPosition",
			Handler:    _EpidemicsAPI_ReportPosition_Handler,
		},
		{
			MethodName: "CountCasesWithinRadius",
			Handler:    _EpidemicsAPI_CountCasesWithinRadius_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "v1/api.proto",
}
