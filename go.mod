module gitlab.com/epidemics/service

go 1.13

require (
	github.com/golang/protobuf v1.4.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.5.2
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.6.0 // indirect
	github.com/rs/zerolog v1.18.0
	gitlab.com/felipemocruha/gonfig v0.1.0
	google.golang.org/genproto v0.0.0-20200507105951-43844f6eee31
	google.golang.org/grpc v1.29.1
)
