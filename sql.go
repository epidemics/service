package main

const ADD_POSITION_OBSERVATION = `
INSERT INTO position_observation VALUES(
 :device_id, :accuracy_meters, ST_MakePoint(:longitude_degrees, :latitude_degrees),
 :speed_meters_per_second, :timestamp_unix_seconds
)
`
const ADD_NEW_SYMPTOMS_REPORT = `
INSERT INTO symptoms_report VALUES(:device_id, :symptoms, true, :reported_at_unix_seconds)
ON CONFLICT (device_id) DO 
 UPDATE SET symptoms = :symptoms,
  symptoms_active = true,
  reported_at_unix_seconds = :reported_at_unix_seconds 
`

const ADD_SYMPTOMS_RECOVERY_REPORT = `
UPDATE symptoms_report SET symptoms_active = false, reported_at_unix_seconds = :reported_at_unix_seconds
  WHERE device_id = :device_id
`

const GET_POINTS_WITHIN_N_METERS = `
SELECT COUNT(*) FROM last_known_position WHERE 
  ST_DWithin(location::geography, ST_MakePoint($1, $2), $3, false)
  AND has_symptoms = true;
`
