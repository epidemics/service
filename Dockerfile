FROM gcr.io/distroless/static

COPY build/service /

ENTRYPOINT ["/service"]
