VERSION="v0.1.0"

proto:
#cd /usr/include && git clone https://github.com/googleapis/googleapis
	@protoc -I proto/ proto/v1/api.proto \
	-I /usr/include/googleapis \
	--include_imports \
	--include_source_info \
	--descriptor_set_out=api/api_descriptor.pb \
	--grpc-java_out=grpc:./api/java \
	--go_out=plugins=grpc:./api/go

envoy:
	@docker run -it --rm --name envoy --network=host \
	-v $(PWD)/api/api_descriptor.pb:/data/api_descriptor.pb:ro \
	-v $(PWD)/envoy_config.yaml:/etc/envoy/envoy.yaml:ro \
	envoyproxy/envoy -l error --config-path /etc/envoy/envoy.yaml

ide:
	adb kill-server && sudo adb start-server && adb devices

postgis:
	docker create volume epidemics-data
	docker run --restart always -d --network host -e POSTGRES_USER=epidemics -e POSTGRES_PASSWORD=epidemics -e POSTGRES_DB=epidemics -e PGDATA=/var/lib/postgresql/data/pgdata -v epidemics-data:/var/lib/postgresql/data/pgdata postgis/postgis:11-3.0

build:
	CGO_ENABLED=0 go build -o build/service -ldflags="-s -w"
	docker build -t registry.gitlab.com/epidemics/service:$(VERSION) -f Dockerfile .

.PHONY: proto
