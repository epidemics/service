package main

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

func NewDatabase(config *DatabaseConfig) (*Database, error) {
	db, err := sqlx.Connect("postgres", makeConnStr(config))
	if err != nil {
		return nil, errors.Wrap(
			err, "failed to create database connection")
	}

	if _, err := db.Exec(SCHEMA); err != nil {
		return nil, errors.Wrap(err, "failed to exec DB schema")
	}

	return &Database{db}, err
}

func makeConnStr(config *DatabaseConfig) string {
	return fmt.Sprintf(
		"host=%s user=%s dbname=%s sslmode=disable password=%s",
		config.Host,
		config.User,
		config.Name,
		config.Password,
	)
}

type Database struct {
	db *sqlx.DB
}

type DatabaseConfig struct {
	Host     string `yaml:"host"`
	User     string `yaml:"user"`
	Name     string `yaml:"name"`
	Password string `yaml:"password"`
}

//TODO: create indices
const SCHEMA = `
CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS position_observation (
  device_id text,
  accuracy_meters double precision,
  location geometry,
  speed_meters_per_second double precision,
  timestamp_unix_seconds int
);

CREATE INDEX IF NOT EXISTS position_index ON position_observation(location);
CREATE INDEX IF NOT EXISTS position_timestamp_index ON position_observation(timestamp_unix_seconds);

CREATE TABLE IF NOT EXISTS symptoms_report (
  device_id text PRIMARY KEY,
  symptoms jsonb,
  symptoms_active bool,
  reported_at_unix_seconds int  
);

CREATE OR REPLACE FUNCTION update_has_symptoms() RETURNS trigger AS $$
  BEGIN
    UPDATE last_known_position SET has_symptoms = NEW.symptoms_active,
      last_modified_at_unix_seconds = EXTRACT(EPOCH FROM NOW())   
      WHERE device_id = NEW.device_id;
    
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_last_has_symptoms ON symptoms_report;
CREATE TRIGGER update_last_has_symptoms AFTER INSERT OR UPDATE ON symptoms_report
  FOR EACH ROW EXECUTE PROCEDURE update_has_symptoms(); 

CREATE TABLE IF NOT EXISTS last_known_position (
  device_id text PRIMARY KEY,
  location geometry,
  has_symptoms bool,
  last_modified_at_unix_seconds int
);

CREATE INDEX IF NOT EXISTS last_position_index ON last_known_position(location);
CREATE INDEX IF NOT EXISTS last_has_symptoms_index ON last_known_position(has_symptoms);

CREATE OR REPLACE FUNCTION position_changed() RETURNS trigger AS $$
  DECLARE symptomatic bool;   
  BEGIN
    symptomatic := (
	SELECT symptoms_active FROM symptoms_report WHERE device_id = NEW.device_id LIMIT 1
    );

    INSERT INTO last_known_position VALUES(NEW.device_id, NEW.location, symptomatic, EXTRACT(EPOCH FROM NOW()))
    ON CONFLICT (device_id) DO
     UPDATE SET location = NEW.location,
      has_symptoms = symptomatic,
      last_modified_at_unix_seconds = EXTRACT(EPOCH FROM NOW()); 
   
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_last_known_position ON position_observation;
CREATE TRIGGER update_last_known_position AFTER INSERT OR UPDATE ON position_observation
  FOR EACH ROW EXECUTE PROCEDURE position_changed();
`
