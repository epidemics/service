package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/felipemocruha/gonfig"
)

const METRICS_ADDR = "0.0.0.0:9057"
const SERVICE_ADDR = "0.0.0.0:53000"

type Config struct {
	DB *DatabaseConfig `yaml:"db"`
}

func main() {
	log.Logger = log.With().Caller().Logger()

	config := &Config{}
	err := gonfig.LoadFromEnv("CONFIG_PATH", config, "config.yaml")
	if err != nil {
		log.Fatal().Msgf("failed to load config: %v", err)
	}

	svc, err := NewService(config)
	if err != nil {
		log.Fatal().Msgf("failed to create service instance: %v", err)
	}
	svc.Run()
}
