package main

import (
	"net"

	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	api "gitlab.com/epidemics/service/api/go/v1"
	"google.golang.org/grpc"
//	"google.golang.org/grpc/credentials"
)

type Service struct {
	server *grpc.Server
	db *Database
}

func (s Service) Run() {
	log.Info().Msgf("Starting server at: %v", SERVICE_ADDR)
	listen, err := net.Listen("tcp", SERVICE_ADDR)
	if err != nil {
		log.Fatal().Msgf("failed to launch server: %v", err)
	}
	api.RegisterEpidemicsAPIServer(s.server, s)
	grpc_prometheus.Register(s.server)

	s.server.Serve(listen)
}

func NewGrpcServer() (*grpc.Server, error) {
	// creds, err := credentials.NewServerTLSFromFile(config.Cert, config.Key)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "failed to load certificates")
	// }

	return grpc.NewServer(
		//		grpc.Creds(creds),
		&grpc.EmptyServerOption{},
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpc_prometheus.StreamServerInterceptor,
			grpc_opentracing.StreamServerInterceptor(),
		)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_prometheus.UnaryServerInterceptor,
			grpc_opentracing.UnaryServerInterceptor(),
		)),
	), nil
}

func NewService(config *Config) (*Service, error) {
	srv, err := NewGrpcServer()
	if err != nil {
		return nil, errors.Wrap(err, "failed to initiliaze server")
	}

	db, err := NewDatabase(config.DB)
	if err != nil {
		return nil, errors.Wrap(err, "failed to connect to database")
	}

	svc := &Service{server: srv, db: db}

	return svc, nil
}
