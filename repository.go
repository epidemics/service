package main

type PositionObservation struct {
	DeviceId string `db:"device_id"`
	AccuracyMeters float64 `db:"accuracy_meters"`
	LongitudeDegrees float64 `db:"longitude_degrees"`
	LatitudeDegrees float64 `db:"latitude_degrees"`
	SpeedMetersPerSecond float64 `db:"speed_meters_per_second"`
	TimestampUnixSeconds int64 `db:"timestamp_unix_seconds"`
}

type NewSymptomsReport struct {
	DeviceId string `db:"device_id"`
	Symptoms []byte `db:"symptoms"` //string?
	ReportedAtUnixSeconds int64 `db:"reported_at_unix_seconds"`
}

type SymptomsRecoveryReport struct {
	DeviceId string `db:"device_id"`
	ReportedAtUnixSeconds int64 `db:"reported_at_unix_seconds"`
}

type CountSymptomaticCases struct {
	Longitude float64 `db:"longitude"`
	Latitude float64 `db:"latitude"`
	RadiusMeters float64 `db:"radius_meters"`
}

func (d Database) AddPositionObservation(observation PositionObservation) error {
 	_, err := d.db.NamedExec(ADD_POSITION_OBSERVATION, observation)
	return err
} 

func (d Database) AddNewSymptomsReport(report NewSymptomsReport) error {
 	_, err := d.db.NamedExec(ADD_NEW_SYMPTOMS_REPORT, report)
	return err
} 

func (d Database) AddSymptomsRecoveryReport(report SymptomsRecoveryReport) error {
 	_, err := d.db.NamedExec(ADD_SYMPTOMS_RECOVERY_REPORT, report)
	return err
} 

func (d Database) CountPointsWithinRadius(long, lat, radius float64) (int, error) {
	var count int
	err := d.db.Get(&count, GET_POINTS_WITHIN_N_METERS, long, lat, radius)
	if err != nil {
		return 0, err
	}

	return count, nil
}
